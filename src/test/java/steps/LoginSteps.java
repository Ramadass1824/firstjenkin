package steps;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.chrome.ChromeDriver;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class LoginSteps {
	public ChromeDriver driver;
	@Given("Open the Browser")
	
	public void openBrowser() {
		System.setProperty("webdriver.chrome.driver", "./drivers\\chromedriver.exe");
		driver = new ChromeDriver();
	}

	
	@Given("Max the Browser")
	public void maxTheBrowser() {
		driver.manage().window().maximize();
	   	    
	}

	@Given("set TimeOut")
	public void setTimeOut() {
		driver.manage().timeouts().implicitlyWait(300, TimeUnit.SECONDS);
		
		
	   
	    
	}

	@Given("Launch the URL")
	public void launchTheURL() {
		driver.get("http://leaftaps.com/opentaps/");
	    
	    
	}

	@Given("Enter the UserName as (.*)")
	public void enterTheUserNameAsDemoSalesManager(String data) {
		driver.findElementById("username").sendKeys(data);
	    	    
	}

	@Given("Enter the Password as (.*)")
	public void enterThePasswordAsCrmfsa(String data) {
		driver.findElementById("password").sendKeys(data);
	    
	    
	}

	@When("click on the Login Button")
	public void clickOnTheLoginButton() {
		driver.findElementByClassName("decorativeSubmit").click();
	    
	    	}

	@Then("Verify the Login")
	public void verifyTheLogin() {
	    System.out.println("Success");
	    
	}

	@Then("click on the crmsfa link text")
	public void clickOnTheCrmsfaLinkText() {
		driver.findElementByLinkText("CRM/SFA").click();
	    
	    
	}

	@Then("click on the Lead Button")
	public void clickOnTheLeadButton() {
		driver.findElementByLinkText("Leads").click();
	    
	    
	}

	@Then("click on the Create Lead")
	public void clickOnTheCreateLead() {
		driver.findElementByLinkText("Create Lead").click();
	    
	}

	@Then("Enter the CompanyName as (.*)")
	public void enterTheCompanyName(String data) {
		driver.findElementById("createLeadForm_companyName").sendKeys(data);
	    
	    
	}

	@Then("Enter the FirstName as (.*)")
	public void enterTheFirstName(String data) {
		driver.findElementById("createLeadForm_firstName").sendKeys(data);
		
	    
	    
	}

	@Then("Enter the LastName as (.*)")
	public void enterTheLastName(String data) {
		driver.findElementById("createLeadForm_lastName").sendKeys(data);
	    
	   
	}

	@When("cilck on the CreateLead Button")
	public void cilckOnTheCreateLeadButton() {
		driver.findElementByName("submitButton").click();
	    
	}

	@Then("verify the FirstName as (.*)")
	public void verifyTheFirstName(String data) {
		String text = driver.findElementById("viewLead_firstName_sp").getText();
		if(text.equals(data))
		{
			System.out.println("True");
			
		}
		else
			System.out.println("False");
	    
	}


}
