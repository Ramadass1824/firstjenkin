package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class CreateLead extends ProjectMethods{
	
	public CreateLead() {
		
		PageFactory.initElements(driver, this);
	}
	@FindBy(id = "createLeadForm_companyName") WebElement elecompname;
	@FindBy(id = "createLeadForm_firstName") WebElement elefirstname;
	@FindBy(id = "createLeadForm_lastName") WebElement elelastname;
	@FindBy(name = "submitButton") WebElement elesubmitbutton;


public CreateLead entercomname(String data)
{
	type(elecompname, data);
	return this;
}

public CreateLead enterfirst(String data)
{
	type(elefirstname, data);
	return this;
}

public CreateLead enterlastname(String data)
{
	
	type(elelastname, data);
	return this;
}

public ViewLead submit()
{
	
	click(elesubmitbutton);
	return new ViewLead();
}
}