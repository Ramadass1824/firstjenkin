package testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import pages.CreateLead;
import pages.HomePage;
import pages.LoginPage;
import pages.MyHomePage;
import pages.MyLead;
import pages.ViewLead;
import utils.DataInputProvider;
import wdMethods.ProjectMethods;

public class TC002_Createlead extends ProjectMethods{
	@BeforeTest
	public void setData() {
		testCaseName = "TC002_CreateLead";
		testDescription = "CreateLead";
		authors = "Ram";
		category = "smoke";
		dataSheetName = "Input";
		testNodes = "Leads";
	}
	
	@Test(dataProvider = "fetchData")
	public void createLeadFunction(String userName,String password,String CompName,String firstName,String lastName) {		
		new LoginPage()
		.enterUserName(userName)
		.enterPassword(password)
		.clickLogin();
		
		 new HomePage()
		 .clickCRMFSA();
		 
		 new MyHomePage()
		 .clicklead();
		 
		 new MyLead()
		 .clickcreatelead();
		 
		 new CreateLead()
		 .entercomname(CompName)
		 .enterfirst(firstName)
		 .enterlastname(lastName)
		 .submit();
		  
		 new ViewLead()
		 .verifyfirstname(firstName);
		 
		 
	
		 
				
		/*LoginPage lp = new LoginPage();
		lp.enterUserName();
		lp.enterPassword();
		lp.clickLogin();*/
	}
	
	@DataProvider(name="fetchData")
	public  Object[][] getData(){
		return DataInputProvider.getSheet(dataSheetName);		
	}	

}
